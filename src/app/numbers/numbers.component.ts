import { Component } from '@angular/core';

@Component({
  selector: 'app-numbers',
  templateUrl: './numbers.component.html',
  styleUrls: ['./numbers.component.css']
})
export class NumbersComponent  {
  numbers: number[]=[];
  // container:

  constructor() {
  }

  generateNumbers() {
    for (let i = 0; i < 5; i++) {
      let n = Math.floor(Math.random() * 36) + 5;
      let check = this.numbers.includes(n);

      if (check === false) {
        this.numbers.push(n);
      } else {
        while (check === true) {
          n = Math.floor(Math.random() * 36) + 5;
          check = this.numbers.includes(n);
          if (check === false) {
            this.numbers.push(n);
          }
        }
      }
      // let num = document.createElement("div");
      // num.className = "chord-box";
      // num.innerHTML = String(this.numbers[i]);
      // body.appendChild(num);
    }
    console.log(this.numbers);
  }
}
